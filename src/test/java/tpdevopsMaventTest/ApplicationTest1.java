/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpdevopsMaventTest;

import java.util.logging.Level;
import java.util.logging.Logger;
import static org.assertj.core.api.Assertions.assertThat;
import com.tpdevopsmaven.Calcul;
import com.tpdevopsmaven.CalculationType;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;

import static com.tpdevopsmaven.Tpdevops.ExecuteCalcul;
import org.junit.jupiter.api.Test;


/**
 *
 * @author morga
 */
public class ApplicationTest1 {
    
    public ApplicationTest1() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    public void MultiplicationTest(){
       
        Calcul calc = new Calcul(10, 10, CalculationType.multiplication);
        float result =  0;
        try {
           result =  ExecuteCalcul(calc);
        } catch (Exception ex) {
            Logger.getLogger(ApplicationTest1.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertThat(result).isEqualTo(100);
    }
    
    @Test
    public void DivisionTest(){
       
        Calcul calc = new Calcul(10, 10, CalculationType.division);
        float result =  0;
        try {
           result =  ExecuteCalcul(calc);
        } catch (Exception ex) {
            Logger.getLogger(ApplicationTest1.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertThat(result).isEqualTo(1);
    }
    
    @Test
    public void DivisionBy0Test(){
       
        Calcul calc = new Calcul(10, 0, CalculationType.division);
        float result =  0;
        String exMessage = "";
        try {
           result =  ExecuteCalcul(calc);
        } catch (Exception ex) {
            exMessage = ex.getMessage();
        }
        assertThat(result).isEqualTo(0);
        assertThat(exMessage).isNotEmpty();

    }
    
    @Test
    public void additionTest(){
       
        Calcul calc = new Calcul(10, 10, CalculationType.addition);
        float result =  0;
        try {
           result =  ExecuteCalcul(calc);
        } catch (Exception ex) {
            Logger.getLogger(ApplicationTest1.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertThat(result).isEqualTo(20);
    }
    
    @Test
    public void soustractionTest(){
       
        Calcul calc = new Calcul(15, 10, CalculationType.soustraction);
        float result =  0;
        try {
           result =  ExecuteCalcul(calc);
        } catch (Exception ex) {
            Logger.getLogger(ApplicationTest1.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertThat(result).isEqualTo(5);
    }
    
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
