/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tpdevopsmaven;

/**
 *
 * @author morga
 */
public class Calcul {
    float a;
    float b;
    CalculationType OperationType;

    public Calcul(float a, float b, CalculationType OperationType) {
        this.a = a;
        this.b = b;
        this.OperationType = OperationType;
    }

    public float getA() {
        return a;
    }

    public void setA(float a) {
        this.a = a;
    }

    public float getB() {
        return b;
    }

    public void setB(float b) {
        this.b = b;
    }

    public CalculationType getOperationType() {
        return OperationType;
    }

    public void setOperationType(CalculationType OperationType) {
        this.OperationType = OperationType;
    }

    @Override
    public String toString() {
        switch(OperationType){
            case addition:
                return a +" + "+ b;
            case division:
                return a +" / "+ b;
            case multiplication:
                return a +" x "+ b;
            case soustraction:
                return a +" - "+ b;
        }
        return a + " "+OperationType.toString() +" "+ b;
    }
         
}
