/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tpdevopsmaven;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author morga
 */
public class Tpdevops {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        List<Calcul> calculationList = new ArrayList<>();
        int i =0;
        for(i=0;i< args.length;i = i+3){
            calculationList.add(getCalcul(args[i],args[i+1].replace("-", ""),args[i+2]));
        }
        
        for(Calcul calc : calculationList){
            try{
                float result = ExecuteCalcul(calc);
                System.out.println(calc.toString() + " = "+ result);
            }catch(Exception ex){
                System.out.println("Une erreur c'est produite lors du calcul suivant : "+calc.toString() + "\n"+ex.getMessage());
            }
        }
        
        if(calculationList.isEmpty()){
            System.out.println("Utilisation : \n"
                             + " 'a -multiplication b' pour effectuer une multiplication de a par b \n"
                             + " 'a -soustraction b' pour effectuer une soustraction de a et b \n"
                             + " 'a -addition b' pour effectuer une addition de a et b \n"
                             + " 'a -division b' pour effectuer une division de a par b \n");
        }
        
    }
    
    public static Calcul getCalcul(String nb1,String operation,String nb2){
        return new Calcul(Float.parseFloat(nb1),Float.parseFloat(nb2), CalculationType.valueOf(operation));
    }
    
    public static float ExecuteCalcul(Calcul calc) throws Exception{
        switch(calc.OperationType){
            case addition:
                return calc.getA() + calc.getB();
            case division:
                if(calc.getB() == 0){
                    throw new Exception("Erreur : La division par 0 est impossible");
                }
                return calc.getA() / calc.getB();
            case multiplication:
                return calc.getA() * calc.getB();
            case soustraction:
                return calc.getA() - calc.getB();
             
        }
        throw new Exception("Erreur : une erreur c'est produite lors du calcul avec l'opérateur : "+calc.OperationType.toString());
    }
}