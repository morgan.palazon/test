FROM maven:4.0.0-jdk-8

WORKDIR /app

COPY . .

RUN mvn clean

RUN mvn package

EXPOSE 80

CMD java -jar ./target/tpdevopsMaven-1.0-SNAPSHOT.jar